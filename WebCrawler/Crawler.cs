using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace WebCrawler
{
  // simple web crawler (scrapper) that find out all the links on certain page
  class Crawler {
    List<string> visitedLinks = new List<string>();

    // Find links in an URL and saving it in a file
    public void GetLinks(string url, int depth = 1, string path = null) {
      url = ProcessURL(url.Trim());
      if(path == null) {
        // Default path will be stored in bin/Debug/URLs.txt
        path = Directory.GetCurrentDirectory() + "\\URLs.txt";
      }
      List<string> URLs = CalculateLinks(url);

      if(URLs != null) {
        // save links
        File.AppendAllText(path, url + ":\r\n");
        File.AppendAllLines(path, URLs);
        File.AppendAllText(path, "\r\n");

        // console display
        Console.WriteLine(url);
        URLs.ForEach(x => Console.WriteLine("└─" + x));
        Console.WriteLine();

        // depth recursion
        if (depth > 1) {
          foreach (var item in URLs) {
            if (this.visitedLinks.Contains(item)) continue;
            GetLinks(item, depth - 1, path);
            this.visitedLinks.Add(item);
          }
        }
      }
    }

    // processing string
    private string ProcessURL(string url) {
      if (url[url.Length - 1] == '/') {
        url = url.TrimEnd(url[url.Length - 1]);
      }
      if(!url.StartsWith("http://", StringComparison.OrdinalIgnoreCase) && !url.StartsWith("https://", StringComparison.OrdinalIgnoreCase)) {
        url = "http://" + url;
      }
      return url;
    }

    // parsing html and making a list of url on a certain page
    private List<string> CalculateLinks(string currentUrl) {
      List<string> URLs = new List<string>();
      Uri host;
      try {
        host = new Uri(currentUrl);
      } catch (Exception) {
        return null;
      }
     
      HtmlWeb hw = new HtmlWeb();
      HtmlDocument doc;
      try {
        doc = hw.Load(host.AbsoluteUri);
      } catch (Exception) {
        return null;
      }

      if (doc.DocumentNode.SelectNodes("//a[@href]") != null) {
        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]")) {
          string url = link.Attributes["href"].Value;
          if (url == "#" || string.IsNullOrEmpty(url)) {
            continue;
          }
          if (Uri.IsWellFormedUriString(url, UriKind.Relative)) {
            try {
              Uri uri = new Uri(host.Scheme + "://" + host.Host + url);
              url = uri.AbsoluteUri;
            } catch (Exception) {
              continue;
            }
          }
          URLs.Add(" " + url);
        }
      }

      return URLs;
    }

  }
}