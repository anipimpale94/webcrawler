using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
  class Program
  {
    static void Main(string[] args) {
      Console.WriteLine("Input Starting URL:");
      string url = Console.ReadLine();

      Crawler crawl = new Crawler();
      crawl.GetLinks(url, 2);

      Console.ReadLine();
    }
  }
}
